package main

import (
	"time"

	"github.com/streadway/amqp"
)

var (
	RMQServ RMQ
)

type RMQ struct {
	Conn          *amqp.Connection
	Chn           *amqp.Channel
	amqpServerURL string
	queueName     string
	exchangeName  string
}

func GetRMQ() *RMQ {
	return &RMQServ
}

func (r *RMQ) InitRMQConnector() error {

	connectRabbitMQ, err := amqp.Dial(r.amqpServerURL)
	if err != nil {
		return err
	}

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		return err
	}
	if err := channelRabbitMQ.ExchangeDeclare(
		r.exchangeName, // name
		"direct",       // type
		true,           // durable
		false,          // auto-deleted
		false,          // internal
		false,          // noWait
		nil,            // arguments
	); err != nil {
		return err
	}

	RMQServ.Conn = connectRabbitMQ
	RMQServ.Chn = channelRabbitMQ
	return nil
}

func (r *RMQ) CloseRMQConnector() {
	r.Chn.Close()
	r.Conn.Close()
}

func (r *RMQ) SendMessage(msg string) error {

	message := amqp.Publishing{
		ContentType: "text/plain",
		Body:        []byte(msg),
		Timestamp:   time.Now(),
	}
	if err := r.Chn.Publish(
		r.exchangeName, // exchange
		r.queueName,    // routing key
		false,          // mandatory
		false,          // immediate
		message,        // message to publish
	); err != nil {
		return err
	}
	return nil
}
