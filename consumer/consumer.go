package main

import (
	"log"
	"os"

	"github.com/streadway/amqp"
)

func main() {

	InitMemoryStore()

	amqpServerURL := os.Getenv("AMQP_SERVER_URL")
	queueName := os.Getenv("RMQ_QUEUE_NAME")
	exchangeName := os.Getenv("RMQ_EXCHANGE_NAME")

	connectRabbitMQ, err := amqp.Dial(amqpServerURL)
	if err != nil {
		panic(err)
	}
	defer connectRabbitMQ.Close()

	channelRabbitMQ, err := connectRabbitMQ.Channel()
	if err != nil {
		panic(err)
	}
	defer channelRabbitMQ.Close()

	err = channelRabbitMQ.ExchangeDeclare(
		exchangeName, // name of the exchange
		"direct",     // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	)
	if err != nil {
		panic(err)
	}

	q, err := channelRabbitMQ.QueueDeclare(
		queueName, // name, leave empty to generate a unique name
		true,      // durable
		false,     // delete when usused
		false,     // exclusive
		false,     // noWait
		nil,       // arguments
	)

	err = channelRabbitMQ.QueueBind(
		q.Name,       // name of the queue
		queueName,    // bindingKey
		exchangeName, // sourceExchange
		false,        // noWait
		nil,          // arguments
	)

	// Subscribing to QueueService1 for getting messages.
	messages, err := channelRabbitMQ.Consume(
		q.Name,          // queue
		"consumer-name", // consumer
		true,            // auto-ack
		false,           // exclusive
		false,           // no-local
		false,           // no-wait
		nil,             // args
	)
	if err != nil {
		log.Println("[ERROR] consume: ", err)
	}

	log.Println("[INFO] consumer successfully connected to RabbitMQ")
	log.Println("[INFO] consumer Waiting for messages")

	// Make a channel to receive messages into infinite loop.
	forever := make(chan bool)

	go func() {
		for message := range messages {
			log.Printf("[INFO] > received message: %s\n", message.Body)
			ParseMessage(message.Body)
		}
	}()
	<-forever
	// defer f.Close()
}
