# Client-Server application

client == producer <br>
server == consumer

## Postman doc API
You can find the scheme testTask.postman_collection.json

## Config
config variables stored in .env file
please create this file based on .env.example

## Run project
From Docker container: <br>
`docker-compose up --build -d` <br>
-d run as demon app, you can skip it if you want to look on logs

## RabbitMQ
after containers ran you can check RabbitMQ statistics via admin page: http://localhost:15672 guest:guest