package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"log"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// commands: AddItem(), RemoveItem(), GetItem(), GetAllItems()
const (
	ADD_ITEM      = "additem"
	REMOVE_ITEM   = "removeitem"
	GET_ITEM      = "getitem"
	GET_ALL_ITEMS = "getallitems"
)

type Message struct {
	Cmd  string
	ID   string
	Item string
}

func HTTPHealthCheck(c *gin.Context) {
	c.String(http.StatusOK, "pong")
}

func AddItem(c *gin.Context) {
	i := c.PostForm("item")
	if i == "" {
		error := fmt.Sprintf("item not found")
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": error})
		return
	}
	id := uuid.New()

	msg := &Message{
		Cmd:  ADD_ITEM,
		ID:   id.String(),
		Item: i,
	}

	msgOut, err := json.Marshal(msg)
	if err != nil {
		log.Printf("[ERROR] cant create a message ", err)
		error := errors.New("cant create a message")
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": error})
		return

	}

	if err := RMQServ.SendMessage(string(msgOut)); err != nil {
		log.Printf("[ERROR] send message %s\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": id})
}

func RemoveItem(c *gin.Context) {
	id := c.PostForm("id")
	if id == "" {
		error := fmt.Sprintf("id not found: %s", id)
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": error})
		return
	}

	msg := &Message{
		Cmd: REMOVE_ITEM,
		ID:  id,
	}

	msgOut, err := json.Marshal(msg)
	if err != nil {
		log.Printf("[ERROR] cant create a message ", err)
		error := fmt.Sprintf("cant create a message")
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": error})
		return

	}

	if err := RMQServ.SendMessage(string(msgOut)); err != nil {
		log.Printf("[ERROR] send message %s\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "request sent"})
}

func GetItem(c *gin.Context) {
	id := c.PostForm("id")
	if id == "" {
		error := fmt.Sprintf("id not found: %s", id)
		c.JSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": error})
		return
	}

	msg := &Message{
		Cmd: GET_ITEM,
		ID:  id,
	}

	msgOut, err := json.Marshal(msg)
	if err != nil {
		log.Printf("[ERROR] cant create a message ", err)
		error := fmt.Sprintf("cant create a message")
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": error})
		return

	}

	if err := RMQServ.SendMessage(string(msgOut)); err != nil {
		log.Printf("[ERROR] send message %s\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "request sent"})
}

func GetAllItems(c *gin.Context) {

	msg := &Message{
		Cmd: GET_ALL_ITEMS,
	}

	msgOut, err := json.Marshal(msg)
	if err != nil {
		log.Printf("[ERROR] cant create a message ", err)
		error := fmt.Sprintf("cant create a message")
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": error})
		return

	}

	if err := RMQServ.SendMessage(string(msgOut)); err != nil {
		log.Printf("[ERROR] send message %s\n", err)
		c.JSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "Internal server error"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "request sent"})
}
