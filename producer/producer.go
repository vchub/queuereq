// ./sender/main.go

package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {

	amqpServerURL := os.Getenv("AMQP_SERVER_URL")
	httpAPIport := os.Getenv("P_HTTP_PORT")
	queueName := os.Getenv("RMQ_QUEUE_NAME")
	exchangeName := os.Getenv("RMQ_EXCHANGE_NAME")

	RMQServ = RMQ{
		amqpServerURL: amqpServerURL,
		queueName:     queueName,
		exchangeName:  exchangeName,
	}

	if err := RMQServ.InitRMQConnector(); err != nil {
		log.Fatalf("[ERROR] Init rabbit mq connector: %s\n", err)
	}

	//------------HTTP server setup
	r := gin.Default()
	r.GET("/ping", HTTPHealthCheck)
	r.POST("/item/add", AddItem)
	r.DELETE("/item/remove", RemoveItem)
	r.POST("/item/get", GetItem)
	r.GET("/items/get", GetAllItems)

	srv := &http.Server{
		Addr:    ":" + httpAPIport,
		Handler: r,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("[ERROR] listen and serve: %s\n", err)
		}
	}()

	//------------Gracefull shutdown
	{
		quit := make(chan os.Signal, 1)
		signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
		<-quit
		log.Println("[INFO]shutdown Server ...")

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		//HTTP Server shutdown
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatal("[INFO] api server shutdown: ", err)
		}

		log.Println("[INFO] server exiting")
	}
}
