package main

import (
	"encoding/json"
	"log"
)

// commands: AddItem(), RemoveItem(), GetItem(), GetAllItems()
const (
	ADD_ITEM      = "additem"
	REMOVE_ITEM   = "removeitem"
	GET_ITEM      = "getitem"
	GET_ALL_ITEMS = "getallitems"
)

type Message struct {
	Cmd  string `json:"Cmd"`
	ID   string `json:"ID"`
	Item string `json:"Item"`
}

func ParseMessage(msg []byte) {
	message := &Message{}
	if err := json.Unmarshal(msg, message); err != nil {
		log.Printf("[ERROR] parse message: %s  \n", string(msg))
	}

	switch message.Cmd {
	case ADD_ITEM:
		AddItem(message.ID, message.Item)
	case REMOVE_ITEM:
		RemoveItem(message.ID)
	case GET_ITEM:
		GetItem(message.ID)
	case GET_ALL_ITEMS:
		GetAllItems()
	default:
		log.Printf("[ERROR] command %s not found  \n", message.Cmd)
	}

}
