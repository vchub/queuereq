package main

import (
	"log"
	"sync"

	om "github.com/elliotchance/orderedmap"
)

type MemoryMap struct {
	Items *om.OrderedMap
	sync.Mutex
}

var (
	MemoryStore MemoryMap
)

func InitMemoryStore() {
	MemoryStore.Items = om.NewOrderedMap()
}
func AddItem(id, msg string) {
	MemoryStore.Lock()
	defer MemoryStore.Unlock()
	log.Printf("[INFO] add item %s\n", id)
	MemoryStore.Items.Set(id, msg)
}
func RemoveItem(id string) {
	MemoryStore.Lock()
	defer MemoryStore.Unlock()
	log.Printf("[INFO] remove item %s\n", id)
	MemoryStore.Items.Delete(id)
}
func GetItem(id string) {
	e := MemoryStore.Items.GetElement(id)
	log.Printf("[INFO] id: %s value: %s \n", id, e.Value)
}
func GetAllItems() {
	log.Println("[INFO] get all items")
	for el := MemoryStore.Items.Front(); el != nil; el = el.Next() {
		log.Println(el.Key, el.Value)
	}
}
